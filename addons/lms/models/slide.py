from odoo import api, fields, models ,_ ,exceptions
import boto3
from botocore.client import Config
import logging
from botocore.exceptions import ClientError

from odoo.addons.http_routing.models.ir_http import slug
from odoo.exceptions import Warning, UserError, AccessError
from odoo.http import request
from odoo.addons.http_routing.models.ir_http import url_for
import os
import botocore
import io
from PIL import Image
from odoo.exceptions import UserError


class Slide(models.Model):
    _inherit = 'slide.slide'

    origen_video = fields.Selection([
        ('youtube', 'youtube'),
        ('aws_s3', 'aws s3'),
        ('googledrive', 'google drive'),
        ],
        string='origen', required=True,
        default='youtube',
        help="youtube (link) , aws s3(ingresar credenciales en compañia), ")

    company_id = fields.Many2one(
        'res.company', 'Company',
        default=lambda self: self.env['res.company']._company_default_get())

    @api.depends('document_id', 'slide_type', 'mime_type')
    def _compute_embed_code(self):
        os.system("echo '%s'" % ("compute embed ----------------------------"))
        base_url = request and request.httprequest.url_root or self.env['ir.config_parameter'].sudo().get_param('web.base.url')
        if base_url[-1] == '/':
            base_url = base_url[:-1]
        for record in self:
            os.system("echo '%s'" % (record))
            if record.datas and (not record.document_id or record.slide_type in ['document', 'presentation']):
                slide_url = base_url + url_for('/slides/embed/%s?page=1' % record.id)
                record.embed_code = '<iframe src="%s" class="o_wslides_iframe_viewer" allowFullScreen="true" height="%s" width="%s" frameborder="0"></iframe>' % (slide_url, 315, 420)
            elif record.slide_type == 'video' or record.document_id:
                if record.origen_video == 'youtube' :
                    # embed youtube video
                    record.embed_code = '<iframe src="//www.youtube.com/embed/%s?theme=light" allowFullScreen="true" frameborder="0"></iframe>' % (record.document_id)
                elif record.origen_video == 'googledrive' :
                    # embed drive video
                    record.embed_code = '<iframe src="//drive.google.com/file/d/%s/preview" allowFullScreen="true" frameborder="0"></iframe>' % (record.document_id)

                elif record.origen_video == 'aws_s3':
                    # embed s3 video
                    os.system("echo '%s'" % ("s3---------------------------------------"))
                    clave = record.url
                    url = record.get_url_s3(clave)[6:]
                    os.system("echo '%s'" % (url))
                    record.document_id = clave
                    record.embed_code = '<iframe src="%s" allowFullScreen="true" frameborder="0"></iframe>' % (url)

                else:
                    record.embed_code = False


            else:
                record.embed_code = 'no hay'

    @api.onchange('url')
    def _on_change_url(self):
        self.ensure_one()
        if self.url and (self.origen_video=='youtube' or self.origen_video=='googledrive'):
            os.system("echo '%s'" % ("onchangeurl ---------------------------------"))
            res = self._parse_document_url(self.url)
            if res.get('error') :
                raise Warning(_('Could not fetch data from url. Document or access right not available:\n%s') % res['error'])
            values = res['values']
            if not values.get('document_id'):
                raise Warning(_('Please enter valid Youtube or Google Doc URL'))
            for key, value in values.items():
                self[key] = value

    def get_aws_client(self, service):
        if self.company_id.aws_access_key is None or self.company_id.aws_secret_key is None:
            raise exceptions.UserError("Configure Access Key and Access Secret")

        return boto3.client(service,
                            aws_access_key_id=self.company_id.aws_access_key,
                            aws_secret_access_key=self.company_id.aws_secret_key,
                            region_name=self.company_id.aws_region)

    def get_aws_session(self):
        if self.company_id.aws_access_key is None or self.company_id.aws_secret_key is None:
            raise UserError("Configurar accesos a AWS")
        return boto3.Session(
            aws_access_key_id=self.company_id.aws_access_key,
            aws_secret_access_key=self.company_id.aws_secret_key,
            region_name=self.company_id.aws_region
        )


    def get_url_s3(self,clave):
        s3_client = self.get_aws_client('s3')
        try:
            response = s3_client.generate_presigned_url('get_object',
                                                    Params={'Bucket': self.company_id.bucket,
                                                            'Key': clave},
                                                    ExpiresIn=3000)
        except ClientError as e:
            logging.error(e)
            return None

        # The response contains the presigned URL
        return response
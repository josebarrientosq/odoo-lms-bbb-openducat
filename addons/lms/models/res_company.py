from odoo import api,models,fields,_ ,exceptions
import boto3
from odoo.exceptions import UserError
import hashlib

class ResCompany(models.Model):
    _inherit = "res.company"

    name = fields.Char(string="Nombre")
    aws_access_key = fields.Char(string="AWS Access Key")
    aws_secret_key = fields.Char(string="AWS Secret Key")
    aws_region = fields.Char(string="AWS Region", default="us-east-2")
    bucket = fields.Char()

    def hash(self):
        raise exceptions.UserError(hashlib.sha1(b"test").hexdigest())

    def get_aws_session(self):
        if self.aws_access_key is None or self.aws_secret_key is None:
            raise UserError("Configurar accesos a AWS")
        return boto3.Session(
            aws_access_key_id=self.aws_access_key,
            aws_secret_access_key=self.aws_secret_key,
            region_name=self.aws_region
        )

    def bucket_existe(self):
        session = self.get_aws_session()
        s3 = session.resource('s3')
        if s3.Bucket(self.bucket) in s3.buckets.all():
            raise UserError("El bucket existe")
        else :
            raise UserError("No se puede conectar al servicio")
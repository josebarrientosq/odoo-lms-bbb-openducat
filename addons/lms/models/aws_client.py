from odoo import models, fields, api, exceptions
import boto3
import botocore
import os
import io
from odoo.exceptions import UserError


class awsclient(models.Model):
    _name = 'aws.client'

    name = fields.Char(string="Nombre")
    aws_access_key = fields.Char(string="AWS Access Key")
    aws_secret_key = fields.Char(string="AWS Secret Key")
    aws_region = fields.Char(string="AWS Region", default="us-east-2")

    bucket = fields.Char()
    dynamo = fields.Char()
    rekognition = fields.Char()



    def get_aws_client(self, service):
        if self.aws_access_key is None or self.aws_secret_key is None:
            raise exceptions.UserError("Configure Access Key and Access Secret")

        return boto3.client(service,
                            aws_access_key_id=self.aws_access_key,
                            aws_secret_access_key=self.aws_secret_key,
                            region_name=self.aws_region)

    def get_aws_session(self,service):
        if self.aws_access_key is None or self.aws_secret_key is None:
            raise exceptions.UserError("Configure Access Key and Access Secret")
        return boto3.Session(
            aws_access_key_id=self.aws_access_key,
            aws_secret_access_key=self.aws_secret_key,
            region_name=self.aws_region
        )


    def list_buckets(self):
        client = self.get_aws_client('s3')
        response = client.list_buckets()

        for bucket in response['Buckets']:
            os.system("echo '%s'" % (bucket["Name"]))


    def upload_images(self):
        images = [('images/ejemplo.jpg', 'ejemplo')
                  ]
        session= self.get_aws_session('s3')
        s3 = session.resource('s3')
        # Iterate through list to upload objects to S3
        for image in images:
            file = open(image[0], 'rb')
            object = s3.Object(self.bucket, 'index/' + image[0])
            ret = object.put(Body=file,
                             Metadata={'FullName': image[1]}
                             )

# -*- coding: utf-8 -*-
import json
import logging
from datetime import datetime
from werkzeug.exceptions import Forbidden, NotFound

from odoo import fields, http, SUPERUSER_ID, tools, _
from odoo.http import request
from odoo.addons.base.models.ir_qweb_fields import nl2br
from odoo.addons.http_routing.models.ir_http import slug
from odoo.addons.payment.controllers.portal import PaymentProcessing
from odoo.addons.website.controllers.main import QueryURL
from odoo.addons.website.models.ir_http import sitemap_qs2dom
from odoo.exceptions import ValidationError
from odoo.addons.website.controllers.main import Website
from odoo.addons.website_form.controllers.main import WebsiteForm
from odoo.osv import expression
from odoo.addons.website_sale.controllers.main import WebsiteSale,TableCompute
_logger = logging.getLogger(__name__)

class EduhubCore(http.Controller):
     @http.route('/mis-grupos/', type='http',auth='public', methods=['GET', 'POST'], website=True, csrf=False)
     def groups(self,**kw):
        mis_grupos = request.env['eduhub.core'].sudo().search([])
        
        return request.render('eduhub_core.content_groups',{"grupos":mis_grupos})

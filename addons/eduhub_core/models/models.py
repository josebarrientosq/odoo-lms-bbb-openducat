# -*- coding: utf-8 -*-

from odoo import models, fields, api

class EduHubGroups(models.Model):
    _name = 'eduhub.core'

    name = fields.Char(string="Nombre del Grupo")
    docente = fields.Many2one('res.users',
                                string="Docente",
                                help='Docente del Grupo')
    
    course = fields.Many2one('slide.channel',
                                string='Curso',
                                help='Curso Relacionado')

    list_course = fields.One2many('slide.channel', 'grupo_id')

class EduHubCourseGroups(models.Model):
    _inherit = 'slide.channel'
    
    grupo_id = fields.Many2one('eduhub.core',
                                string="Group",
                                help='Grupo del Curso')
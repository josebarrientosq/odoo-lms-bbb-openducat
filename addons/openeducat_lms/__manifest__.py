# -*- coding: utf-8 -*-
{
    'name': "openeducat_lms",

    'summary': """
        Short (1 phrase/line) summary of the module's purpose, used as
        subtitle on modules listing or apps.openerp.com""",

    'description': """
        Long description of module's purpose
    """,

    'author': "My Company",
    'website': "http://www.yourcompany.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/13.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','openeducat_core','openeducat_admission','website_slides','website_profile','bigbluebutton'],

    # always loaded
    'data': [
        'data/website_data.xml',
        # 'security/ir.model.access.csv',
        'views/views.xml',
        'views/templates.xml',
        'views/batch_view.xml',
        'views/student_view.xml',
        'views/faculty_view.xml',
        'views/partner_view.xml',
        'views/website_grupo_templates_homepage.xml'
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}

# -*- coding: utf-8 -*-
from odoo import http, tools, _
from odoo.addons.http_routing.models.ir_http import slug
from odoo.addons.website_profile.controllers.main import WebsiteProfile
from odoo.addons.website.models.ir_http import sitemap_qs2dom
from odoo.exceptions import AccessError, UserError
from odoo.http import request
from odoo.osv import expression
import os

class WebsiteGrupos(http.Controller):

    @http.route('/grupos', type='http', auth="user", website=True)
    def mis_grupos(self, **post):
        """ Home page for eLearning platform. Is mainly a container page, does not allow search / filter. """

        if not request.env.user._is_public():
            if request.env.user.is_student:
                mis_grupos = request.env.user.partner_id.student_id[0].batch_ids
            if request.env.user.is_faculty:
                mis_grupos = request.env.user.partner_id.faculty_id[0].batch_ids

        values ={
            'mis_grupos': mis_grupos,
        }

        return request.render('openeducat_lms.grupo_home', values)

    @http.route('/grupo/<model("op.batch"):grupo>', type='http', auth="public", website=True)
    def generar_url_bbb(self, grupo, **post):
        if not request.env.user._is_public():
            if request.env.user.is_faculty: #si es profesor inicia asigna un servidor
                grupo.bbb_meet.asignar_servidor()

                if grupo.bbb_meet.server_id: # si se asigna generar las url, crear reunion y unirse
                    grupo.bbb_meet.accion_crear_reunion()

                    url = grupo.bbb_meet.generar_url_unirse_reunion_moderador_nombre(request.env.user.name)
                    return request.redirect(url)

            if request.env.user.is_student:
                if grupo.bbb_meet.server_id:
                    url = grupo.bbb_meet.generar_url_unirse_reunion_usuario_nombre(request.env.user.name)
                    return request.redirect(url)

    @http.route('/test/callback', type='http',method=["POST"],csrf=False)
    def test_callback(self, **post):
        os.system("echo '%s'" % ("-------------callback----------------------"))
        os.system("echo '%s'" % (post))
        os.system("echo '%s'" % ("la data"))
        data = post.get("data")
        os.system("echo '%s'" % (data[id]))

    @http.route('/test2/callback', type='http',  method=["POST"],csrf=False )
    def test2_callback(self, **post):
        os.system("echo '%s'" % ("-------------callback----------------------"))
        dni = post.get("dni")
        os.system("echo '%s'" % (dni))
        password = post.get("password")
        os.system("echo '%s'" % (password))

    @http.route('/login', auth='public')
    def login(self, **kwargs):
        return request.render('bigbluebutton.login')

"""
       event={"data":{"type":"event",
                       "id": "meeting-screenshare-stopped",
                        "attributes":{
                            "meeting":{
                                "internal-meeting-id" : "dssdgasga",
                                "external-meeting-id": "SEC1A",
                                "user":{
                                    "user-id": "ssdgsddsga",
                                    "name": "nombre",
                                    "role" : "moderator"
                                }

                            }

                        },
                        "event":{"ts":452345245
                                 }
                       }
               }
"""
# class OpeneducatLms(http.Controller):
#     @http.route('/openeducat_lms/openeducat_lms/', auth='public')
#     def index(self, **kw):
#         return "Hello, world"

#     @http.route('/openeducat_lms/openeducat_lms/objects/', auth='public')
#     def list(self, **kw):
#         return http.request.render('openeducat_lms.listing', {
#             'root': '/openeducat_lms/openeducat_lms',
#             'objects': http.request.env['openeducat_lms.openeducat_lms'].search([]),
#         })

#     @http.route('/openeducat_lms/openeducat_lms/objects/<model("openeducat_lms.openeducat_lms"):obj>/', auth='public')
#     def object(self, obj, **kw):
#         return http.request.render('openeducat_lms.object', {
#             'object': obj
#         })

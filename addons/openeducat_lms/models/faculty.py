# -*- coding: utf-8 -*-
###############################################################################
#
#    Tech-Receptives Solutions Pvt. Ltd.
#    Copyright (C) 2009-TODAY Tech-Receptives(<http://www.techreceptives.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

from odoo import models, fields, api, _
from odoo.exceptions import ValidationError
import os

class OpFaculty(models.Model):
    _inherit = "op.faculty"

    batch_ids = fields.Many2many(comodel_name='op.batch',relation='batch_faculty_rel',string='grupos')


    def create_faculty_user(self):
        users_res = self.env['res.users']

        for record in self:
            if not record.user_id:
                user_id = users_res.create({
                    'name': record.name,
                    'login': record.email,
                    'partner_id': record.partner_id.id,
                    'is_faculty': True,
                    'groups_id': [(6, 0, [self.env.ref('base.group_portal').id])],
                    'tz': self._context.get('tz'),
                })
                record.user_id = user_id

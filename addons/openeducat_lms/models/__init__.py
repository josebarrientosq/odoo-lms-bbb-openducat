# -*- coding: utf-8 -*-

from . import models
from . import batch
from . import admission
from . import student
from . import faculty
from . import partner
# -*- coding: utf-8 -*-
###############################################################################
#
#    Tech-Receptives Solutions Pvt. Ltd.
#    Copyright (C) 2009-TODAY Tech-Receptives(<http://www.techreceptives.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

from odoo import models, fields, api, _
from odoo.exceptions import ValidationError


class OpBatch(models.Model):
    _inherit = ["op.batch"]

    group_id = fields.Many2one('res.groups', 'Grupo')
    image_1024 = fields.Image("Image", max_width=1024, max_height=1024)

    bbb_meet = fields.Many2one('bbb.meet','Videoconferencia')
    clave_moderador = fields.Char('clave moderador')
    clave_usuario = fields.Char('clave usuario')

    faculty_ids = fields.Many2many(comodel_name='op.faculty',relation='batch_faculty_rel')
    student_ids = fields.Many2many(comodel_name='op.student', relation='batch_student_rel')


    def asignar_grupo(self):
        grupo = self.env['res.groups'].search([('name','=',self.code)])

        if not grupo:
            portal_group = self.env.ref("base.group_portal")
            self.group_id = self.env['res.groups'].create({
                'name' : self.code,
            })

    def asignar_videoconferencia(self):
        bbb_meet = self.env['bbb.meet'].search([('meeting_id','=',self.code)])
        value = {
                'meeting_id': self.code,
                'name' : self.name,
                'clave_moderador' : self.clave_moderador,
                'clave_usuario' : self.clave_usuario
                }
        if not bbb_meet :
            self.bbb_meet = self.env['bbb.meet'].create(value)
        else :
            bbb_meet.write(value)


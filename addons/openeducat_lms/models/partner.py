from odoo import models, fields


class partner_inherit(models.Model):
    _inherit = 'res.partner'

    is_parent = fields.Boolean("Es padre de familia")
    is_student = fields.Boolean("Es estudiante")
    is_faculty = fields.Boolean("Es docente")

    student_id = fields.One2many('op.student','partner_id')
    faculty_id = fields.One2many('op.faculty','partner_id')

from odoo import api,models,fields,_ ,exceptions
import boto3
from odoo.exceptions import UserError
import hashlib

class ResCompany(models.Model):
    _inherit = "res.company"

    bbb_server = fields.Char('Server')
    bbb_secret_key = fields.Char('Secret Key')

    #bbb_welcome = fields.Html('mensaje bienvenida',default='Para usar la camara y microfono activarlo en la parte inferior '
    #                                                   '<b>Utilice audifonos para evitar escuchar el eco de las conversaciones ')



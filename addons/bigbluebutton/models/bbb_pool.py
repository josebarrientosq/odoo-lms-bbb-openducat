from odoo import models, fields, api , exceptions
from odoo.exceptions import ValidationError, UserError
import boto3
from botocore.exceptions import ClientError
import os
import requests

class bbb_pool(models.Model):
    _name = 'bbb.pool'
    _description = 'administrar pool de servidores ec2 '

    name = fields.Char(string="Nombre")
    server_ids = fields.One2many('bbb.aws','pool_id',string='servidores')
    meet_ids = fields.One2many('bbb.meet','pool_id')

    instance_type = fields.Char('instancia tipo',default='t3a.large')
    max_spot_price = fields.Char('max spot price',default='0.023')
    dominio = fields.Char('dominio')
    subdominio = fields.Char('subdominio')

    max_servidores = fields.Integer('max servidores')
    meet_por_servidor = fields.Integer('reuniones por servidor',default=4)
    min_servidores_iniciar_otro = fields.Integer('min servidores para iniciar otro',default=2)
    callback_url = fields.Char('callback url')

    aws_access_key = fields.Char(string="AWS Access Key")
    aws_secret_key = fields.Char(string="AWS Secret Key")
    aws_region = fields.Char(string="AWS Region", default="us-east-2")

    bbb_welcome = fields.Char(default='Bienvenido al salon. Porfavor para evitar interferencias usar auriculares y mantener el microfono desactivado.Para conexiones estables usar internet por cable ,gracias.')
    maxParticipants = fields.Integer('Max. participantes')
    webcamsOnlyForModerator = fields.Boolean('webcam solo moderador')
    muteOnStart = fields.Boolean('mute on Start')
    allowModsToUnmuteUsers = fields.Boolean('Unmute to user')
    lockSettingsDisableCam = fields.Boolean('desactivar camara')
    lockSettingsDisableMic = fields.Boolean('desactivar microfono')
    allowStartStopRecording = fields.Boolean('permitir grabar')

    def preconfigurar_servidores(self):
        for i in range(self.max_servidores):
            self.env['bbb.aws'].create({
                'name': self.subdominio+str(i)+'.'+self.dominio,
                'instance_type' : self.instance_type,
                'max_spot_price' : self.max_spot_price,
                'record': self.subdominio+str(i)+'.'+self.dominio,
                'pool_id' : self.id
            })

    def get_status(self):
        for servidor in self.server_ids:
            servidor.get_status()

    def crear_servidor_spot(self):
        for servidor in self.server_ids:
            servidor.crear_servidor_spot()

    def get_request_spot(self):
        for servidor in self.server_ids:
            servidor.get_spot_request()

    def iniciar_servidores(self):
        for servidor in self.server_ids:
            servidor.iniciar_ec2()

    def detener_servidores(self):
        for servidor in self.server_ids:
            servidor.detener_ec2()

    def get_ip_elastica(self):
        for servidor in self.server_ids:
            servidor.asociar_ip_elastica()

    def crear_record(self):
        for servidor in self.server_ids:
            servidor.crear_record()

    def instalar_bbb(self):
        for servidor in self.server_ids:
            servidor.instalar_bbb()

    def get_api_secret(self):
        for servidor in self.server_ids:
            servidor.get_apisecret()

    def get_ip_publica(self):
        for servidor in self.server_ids:
            servidor.get_ip_publica()

    def crear_servidores_on_demand(self):
        for servidor in self.server_ids:
            servidor.crear_servidor()

    #enciende 1 servidor
    def start_one_servidor(self):
        for servidor in self.server_ids:
            if servidor.estado == 'off':
                servidor.iniciar_ec2()# iniciar servidor
                return servidor

        os.system("echo '%s'" % ("no hay mas servidores en el pool"))


    def buscar_servidor_disponible(self):
        for servidor in self.server_ids:
            os.system("echo '%s'" % (servidor.name))
            os.system("echo '%s'" % (len(servidor.meet_ids)))
            if len(servidor.meet_ids)< self.meet_por_servidor and servidor.estado == 'on':
                servidor.get_status()
                os.system("echo '%s'" % ('-----------------'+servidor.system_status+'-----------------'))
                if len(servidor.meet_ids)== self.min_servidores_iniciar_otro: #servidor casi lleno , previene que se queden sin servidor
                    self.start_one_servidor()
                #if servidor.system_status == 'ok': # sistema ok
                return servidor

        os.system("echo '%s'" % ("no hay servidor disponible , habilitando ... esperar un momento"))
        return self.start_one_servidor()


    def test_callback(self):
        r = requests.post("localhost:8065/test/callback", data=[{"data":{"type":"event","id":"meeting-screenshare-stopped","attributes":{"meeting":{"internal-meeting-id":"5ed27ea19f46bdac410b8b1803e9f3febc5ec981-1589587385461","external-meeting-id":"SEC1A"}},"event":{"ts":1589587536100}}}])
        os.system("echo '%s'" % (r))

    def test2(self):
        data={
            "dni": 42342,
            "password": 263462,
        }
        r = requests.post("http://localhost:8065/test2/callback", data)
        os.system("echo '%s'" % (r))

from odoo import models, fields, api , exceptions
from odoo.exceptions import ValidationError, UserError
import hashlib
import urllib.parse
import requests
import xml.etree.ElementTree as ET
import os
import boto3
import time

class bbb_meet(models.Model):
    _name = 'bbb.meet'
    _description = 'salas de reuniones'

    meeting_id = fields.Char('codigo')
    internal_meeting_id = fields.Char('codigo interno')
    name = fields.Char('Nombre')
    clave_moderador = fields.Char('clave moderador')
    clave_usuario = fields.Char('clave usuario')
    url_create = fields.Char('url crear')
    url_join_moderador = fields.Char('url unirse moderador')
    url_join_usuario = fields.Char('url unirse usuario')
    url_isMeettingRunning = fields.Char('url esta la reunion en curso?')
    url_getMeetingInfo = fields.Char('url informacion reunion')
    url_end = fields.Char('url terminar reunion')
    url_hooks_create = fields.Char('url crear hook')
    url_hooks_list = fields.Char('url listar hook')

    bucket = fields.Char()
    grabacion = fields.Char()

    pool_id = fields.Many2one('bbb.pool','pool')
    server_id = fields.Many2one('bbb.aws','servidor')

    current_user = fields.Many2one('res.users', 'Usuario actual', default=lambda self: self.env.user)


    def asignar_servidor(self):
        if not self.server_id:
            os.system("echo '%s'" % ("asignar servidor"))
            self.server_id = self.pool_id.buscar_servidor_disponible()

    def deasignar_servidor(self):
        if len(self.server_id.meet_ids) == 1 : #unica conexion
            self.server_id.detener_ec2() #apagar servidor

        self.server_id = None

    def generar_url_crear_reunion(self):
        url= 'https://'+self.server_id.name+'/bigbluebutton/api/'
        api_call='create'
        clave= self.server_id.api_secret
        query = 'name='+urllib.parse.quote(self.name)+'&'+'meetingID='+self.meeting_id+'&'+'moderatorPW='+self.clave_moderador+'&'+'attendeePW='+self.clave_usuario+'&'+'welcome='+urllib.parse.quote(self.pool_id.bbb_welcome)+'&'+'allowStartStopRecording=true'+'&'+'record=true'

        query2= api_call+query+clave
        checksum= hashlib.sha1(query2.encode()).hexdigest()

        self.url_create= url+api_call+'?'+query+'&checksum='+checksum
        return self.url_create

    def generar_url_unirse_reunion_moderador(self):
        url= 'https://'+self.server_id.name+'/bigbluebutton/api/'
        api_call='join'
        clave= self.server_id.api_secret
        query = 'password='+self.clave_moderador+'&'+'fullName='+self.current_user.name+'&'+'meetingID='+self.meeting_id
        query2= api_call+query+clave
        checksum= hashlib.sha1(query2.encode()).hexdigest()

        self.url_join_moderador= url+api_call+'?'+query+'&checksum='+checksum
        return self.url_join_moderador

    def generar_url_unirse_reunion_moderador_nombre(self,nombre):
        url= 'https://'+self.server_id.name+'/bigbluebutton/api/'
        api_call='join'
        clave= self.server_id.api_secret
        query = 'password='+self.clave_moderador+'&'+'fullName='+urllib.parse.quote(nombre)+'&'+'meetingID='+self.meeting_id
        query2= api_call+query+clave
        checksum= hashlib.sha1(query2.encode()).hexdigest()

        url_join_moderador= url+api_call+'?'+query+'&checksum='+checksum
        return url_join_moderador

    def generar_url_unirse_reunion_usuario(self):
        url= 'https://'+self.server_id.name+'/bigbluebutton/api/'
        api_call='join'
        clave= self.server_id.api_secret
        query = 'password='+self.clave_usuario+'&'+'fullName='+self.current_user.name+'&'+'meetingID='+self.meeting_id
        query2= api_call+query+clave
        checksum= hashlib.sha1(query2.encode()).hexdigest()

        self.url_join_usuario= url+api_call+'?'+query+'&checksum='+checksum
        return self.url_join_usuario

    def generar_url_unirse_reunion_usuario_nombre(self,nombre):
        url= 'https://'+self.server_id.name+'/bigbluebutton/api/'
        api_call='join'
        clave= self.server_id.api_secret
        query = 'password='+self.clave_usuario+'&'+'fullName='+urllib.parse.quote(nombre)+'&'+'meetingID='+self.meeting_id
        query2= api_call+query+clave
        checksum= hashlib.sha1(query2.encode()).hexdigest()

        url_join_usuario= url+api_call+'?'+query+'&checksum='+checksum
        return url_join_usuario

    def generar_url_es_reunion_curso(self):
        url= 'https://'+self.server_id.name+'/bigbluebutton/api/'
        api_call = 'isMeetingRunning'
        clave= self.server_id.api_secret
        query = 'meetingID=' + self.meeting_id
        query2 = api_call + query + clave
        checksum = hashlib.sha1(query2.encode()).hexdigest()

        self.url_isMeettingRunning = url + api_call + '?' + query + '&checksum=' + checksum
        return self.url_isMeettingRunning

    def generar_url_ver_informacion_reunion(self):
        url= 'https://'+self.server_id.name+'/bigbluebutton/api/'
        api_call = 'getMeetingInfo'
        clave= self.server_id.api_secret
        query = 'password=' + self.clave_moderador + '&' + 'meetingID=' + self.meeting_id
        query2 = api_call + query + clave
        checksum = hashlib.sha1(query2.encode()).hexdigest()

        self.url_getMeetingInfo = url + api_call + '?' + query + '&checksum=' + checksum
        return self.url_getMeetingInfo

    def generar_url_terminar_reunion(self):
        url= 'https://'+self.server_id.name+'/bigbluebutton/api/'
        api_call = 'end'
        clave= self.server_id.api_secret
        query = 'password=' + self.clave_moderador + '&' + 'meetingID=' + self.meeting_id
        query2 = api_call + query + clave
        checksum = hashlib.sha1(query2.encode()).hexdigest()

        self.url_end = url + api_call + '?' + query + '&checksum=' + checksum
        return self.url_end

    def generar_url_hooks_create(self):
        url = 'https://' + self.server_id.name + '/bigbluebutton/api/'
        api_call = 'hooks/create'
        clave = self.server_id.api_secret
        query = 'callbackURL=' + urllib.parse.quote(self.pool_id.callback_url, safe='')
        #query2 = urllib.parse.quote(self.pool_id.callback_url, safe='') + clave
        #query2 = urllib.parse.quote(self.pool_id.callback_url, safe='') + clave
        query2 = api_call+query+clave
        checksum = hashlib.sha1(query2.encode()).hexdigest()

        self.url_hooks_create = url + api_call + '?' + query + '&checksum=' + checksum
        return self.url_hooks_create

    def generar_url_hooks_list(self):
        url = 'https://' + self.server_id.name + '/bigbluebutton/api/'
        api_call = 'hooks/list'
        clave = self.server_id.api_secret
        query = 'callbackURL=' + urllib.parse.quote(self.pool_id.callback_url, safe='')
        #query2 = query + clave
        #query2= self.pool_id.callback_url+query+clave
        #query2 = self.pool_id.callback_url+clave

        #query2= urllib.parse.quote(self.pool_id.callback_url, safe='')+clave
        #query2 = urllib.parse.quote(self.pool_id.callback_url, safe='')+query + clave
        query2= api_call+ query+clave

        #sha1(<callback URL>+<data body>+<shared secret>)


        checksum = hashlib.sha1(query2.encode()).hexdigest()

        self.url_hooks_list = url + api_call + '?' + query + '&checksum=' + checksum
        return self.url_hooks_list

    def generar_url(self):
        self.generar_url_crear_reunion()
        self.generar_url_unirse_reunion_moderador()
        self.generar_url_unirse_reunion_usuario()
        self.generar_url_es_reunion_curso()
        self.generar_url_ver_informacion_reunion()
        self.generar_url_terminar_reunion()
        self.generar_url_hooks_create()
        self.generar_url_hooks_list()

    def accion_crear_reunion(self):
        url = self.generar_url_crear_reunion()

        r = requests.get(url=url)

        e = ET.fromstring(r.content)
        os.system("echo '%s'" % (url))
        os.system("echo '%s'" % (e))
        os.system("echo '%s'" % (e.find('returncode').text))
        os.system("echo '%s'" % (e.find('internalMeetingID').text))

    def accion_unirse_reunion(self):
        url = self.generar_url_unirse_reunion_moderador()

        return requests.get(url=url)


    def ver_informacion_reunion(self):
        url = self.generar_url_ver_informacion_reunion()
        r=requests.get(url=url)
        e = ET.fromstring(r.content)
        raise UserError(r.text)

    def get_internal_meet(self):
        url = self.generar_url_ver_informacion_reunion()
        r=requests.get(url=url)
        e = ET.fromstring(r.content)
        self.internal_meeting_id = e.find('internalMeetingID').text


    def get_aws_client(self, service):
        if self.pool_id.aws_access_key is None or self.pool_id.aws_secret_key is None:
            raise exceptions.UserError("Configure Access Key and Access Secret")

        return boto3.client(service,
                            aws_access_key_id=self.pool_id.aws_access_key,
                            aws_secret_access_key=self.pool_id.aws_secret_key,
                            region_name=self.pool_id.aws_region)


    def live_recording(self):
        ssm = self.get_aws_client('ssm')
        commands = ["node /bbb-recorder/liveJoin.js '"+self.generar_url_unirse_reunion_moderador_nombre("grabando")+"' liveRecord5.webm 0 true"]
        instance_ids = [self.server_id.instance_id]
        response = ssm.send_command(
            DocumentName="AWS-RunShellScript",  # One of AWS' preconfigured documents
            Parameters={'commands': commands},
            InstanceIds=instance_ids,
        )
        os.system("echo '%s'" % (response))

        command_id = response['Command']['CommandId']

        time.sleep(2)
        output = ssm.get_command_invocation(
            CommandId=command_id,
            InstanceId=self.server_id.instance_id,
        )
        os.system("echo '%s'" % (output))

    def upload_images(self):
        images = [('images/camila.jpg', 'camila')
                  ]
        session= self.get_aws_session('s3')
        s3 = session.resource('s3')
        # Iterate through list to upload objects to S3
        for image in images:
            file = open(image[0], 'rb')
            object = s3.Object(self.bucket, 'index/' + image[0])
            ret = object.put(Body=file,
                             Metadata={'FullName': image[1]}
                                     )

    def upload_recording(self):
        s3 = boto3.resource('s3')
        filename = "/var/www/bigbluebutton-default/record/"+self.grabacion+".mp4"
        s3.meta.client.upload_file(filename, self.bucket, self.grabacion)

"""
Create the entire query string for your API call without the checksum parameter.
    Example for create meeting API call: name=Test+Meeting&meetingID=abc123&attendeePW=111222&moderatorPW=333444
Prepend the API call name to your string
    Example for above query string:
        API call name is create
        String becomes: createname=Test+Meeting&meetingID=abc123&attendeePW=111222&moderatorPW=333444
Now, append the shared secret to your string
    Example for above query string:
        shared secret is 639259d4-9dd8-4b25-bf01-95f9567eaf4b
        String becomes: createname=Test+Meeting&meetingID=abc123&attendeePW=111222&moderatorPW=333444639259d4-9dd8-4b25-bf01-95f9567eaf4b
Now, find the SHA-1 sum for that string (implementation varies based on programming language).
    the SHA-1 sum for the above string is: 1fcbb0c4fc1f039f73aa6d697d2db9ba7f803f17
Add a checksum parameter to your query string that contains this checksum.
    Above example becomes: name=Test+Meeting&meetingID=abc123&attendeePW=111222&moderatorPW=333444&checksum=1fcbb0c4fc1f039f73aa6d697d2db9ba7f803f17
"""

"""
callback
sha1(<callback URL>+<data body>+<shared secret>)

Where:

    <callback URL>: The original callback URL, that doesn’t include the checksum.
    <data body as a string>: All the data sent in the body of the request, concatenated and joined by &, as if they were parameters in a URL.
    <shared secret>: The shared secret of the BigBlueButton server.

"""
from odoo import models, fields, api , exceptions
from odoo.exceptions import ValidationError, UserError
import boto3
from botocore.exceptions import ClientError
import os
import time



class bbb_server(models.Model):
    _name = 'bbb.aws'
    _description = 'administrar servidores ec2'


    pool_id= fields.Many2one('bbb.pool',string='Pool')
    meet_ids = fields.One2many('bbb.meet','server_id')

    name = fields.Char(string="Nombre")
    request_id = fields.Char('request_id')
    instance_id = fields.Char('instancia_id')
    instance_type = fields.Char('instancia tipo')
    max_spot_price = fields.Char('max spot price')
    comando_status = fields.Char('comando status')
    comando = fields.Char('comando')
    zona = fields.Char('zona')
    record = fields.Char('record')
    ip = fields.Char('IP')
    api_secret = fields.Char('api secret')
    command_id = fields.Char()
    es_ip_elastica = fields.Boolean('Es ip elastica')

    estado = fields.Selection([('off', 'off'),
                               ('on', 'on')],
                               default='off')


    instance_status = fields.Selection([
                                    ('pending', 'pending'),
                                    ('running', 'running'),
                                    ('shutting-down', 'shutting-down'),
                                    ('terminated', 'terminated'),
                                    ('stopping', 'stopping'),
                                    ('stopped', 'stopped')],
                                    default='stopped')

    system_status = fields.Selection([
                                    ('ok', 'ok'),
                                    ('impaired', 'impaired'),
                                    ('insufficient-data', 'insufficient-data'),
                                    ('not-applicable', 'not-applicable'),
                                    ('initializing', 'initializing')],
                                    default='insufficient-data')

    company_id = fields.Many2one(
        'res.company', 'Company',
        default=lambda self: self.env['res.company']._company_default_get())

    instanceDict = None

    def get_aws_client(self, service):
        if self.pool_id.aws_access_key is None or self.pool_id.aws_secret_key is None:
            raise exceptions.UserError("Configure Access Key and Access Secret")

        return boto3.client(service,
                            aws_access_key_id=self.pool_id.aws_access_key,
                            aws_secret_access_key=self.pool_id.aws_secret_key,
                            region_name=self.pool_id.aws_region)

    def get_aws_session(self):
        if self.company_id.aws_access_key is None or self.pool_id.aws_secret_key is None:
            raise exceptions.UserError("Configure Access Key and Access Secret")
        return boto3.Session(
            aws_access_key_id=self.pool_id.aws_access_key,
            aws_secret_access_key=self.pool_id.aws_secret_key,
            region_name=self.pool_id.aws_region
        )

    def get_status(self):
        ec2Client = self.get_aws_client('ec2')
        response = ec2Client.describe_instance_status(
            InstanceIds=[
                self.instance_id,
            ],
            DryRun=False,
        )
        os.system("echo '%s'" % (response))
        if self.estado == 'on':
            self.instance_status = response['InstanceStatuses'][0]['InstanceState']['Name']
            self.system_status = response['InstanceStatuses'][0]['SystemStatus']['Status']


    def crear_servidor_spot(self):
        ec2Client = self.get_aws_client('ec2')
        dryRun = False

        response = ec2Client.request_spot_instances(
            DryRun=dryRun,
            InstanceCount=1,
            LaunchSpecification={
                'SecurityGroupIds' : ['sg-0c42b50b83ee74a02'],
                'ImageId' : "ami-03ffa9b61e8d2cfda",
                'InstanceType' : self.instance_type,
                'KeyName' : "escool-key",
            },
            SpotPrice=self.max_spot_price,
            Type='persistent',
            InstanceInterruptionBehavior='stop'
        )
        os.system("echo '%s'" % (response))
        self.estado = 'on'
        self.request_id = response['SpotInstanceRequests'][0]['SpotInstanceRequestId']

    def get_spot_request(self):
        ec2Client = self.get_aws_client('ec2')
        dryRun = False
        response = ec2Client.describe_spot_instance_requests(
            DryRun=dryRun,
            SpotInstanceRequestIds=[
                self.request_id,
            ],
        )
        os.system("echo '%s'" % (response))
        self.instance_id = response['SpotInstanceRequests'][0]['InstanceId']


    def get_ip_publica(self):
        ec2Client = self.get_aws_client('ec2')
        if self.estado == 'on':
            response = ec2Client.describe_instances(
                InstanceIds=[
                    self.instance_id,
                ],
                DryRun=False,
            )
            os.system("echo '%s'" % (response))
            self.ip = response['Reservations'][0]['Instances'][0]['NetworkInterfaces'][0]['Association']['PublicIp']

    def crear_servidor(self):

        dryRun = False  # useful variable to put the script into dry run mode where the function allows it

        ec2Client = self.get_aws_client('ec2')
        session = self.get_aws_session()
        ec2Resource = session.resource('ec2')

        # Create the instance
        instanceDict = ec2Resource.create_instances(
            DryRun=dryRun,
            ImageId="ami-03ffa9b61e8d2cfda",
            KeyName="escool-key",
            InstanceType="t3a.medium",
            SecurityGroupIds=['sg-0c42b50b83ee74a02'],
            MinCount=1,
            MaxCount=1
        )

        self.instance_id = instanceDict[0].id
        # Wait for it to launch before assigning the elastic IP address
        instanceDict[0].wait_until_running()

        self.estado = 'on'

        # Allocate an elastic IP
        eip = ec2Client.allocate_address(DryRun=dryRun, Domain='vpc')
        # Associate the elastic IP address with the instance launched above
        ec2Client.associate_address(
            DryRun=dryRun,
            InstanceId=instanceDict[0].id,
            AllocationId=eip["AllocationId"])

        self.ip = eip["PublicIp"]

        route53Client = self.get_aws_client('route53')
        # Now add the route 53 record set to the hosted zone for the domain
        route53Client.change_resource_record_sets(
            HostedZoneId='Z093093132ENBV1TCQXVM',
            ChangeBatch={
                'Comment': 'Add new instance to Route53',
                'Changes': [
                    {
                        'Action': 'CREATE',
                        'ResourceRecordSet': {
                            'Name': self.record,
                            'Type': 'A',
                            'TTL': 60,
                            'ResourceRecords': [
                                {
                                    'Value': eip["PublicIp"]
                                },
                            ],
                        }
                    },
                ]
            })


    def iniciar_ec2(self):
        ec2 = self.get_aws_client('ec2')
        waiter = ec2.get_waiter('instance_running')
        try:
            response = ec2.start_instances(InstanceIds=[self.instance_id], DryRun=False)
            os.system("echo '%s'" % (response))

            if not self.es_ip_elastica: # actualiza la ip publica, no es necesaria si es elastica
                self.get_ip_publica()
                time.sleep(1)
                self.crear_record()

                os.system("echo '%s'" % ("------------waiting start ec2----------"))
                waiter.wait(
                    InstanceIds=[
                        self.instance_id,
                    ],
                    DryRun=False,
                    WaiterConfig={
                        'Delay': 3,
                        'MaxAttempts': 20
                    }
                )
                os.system("echo '%s'" % ("------------instancia running----------"))


            self.estado = 'on'
            self.instance_status = 'running'




        except ClientError as e:
            os.system("echo '%s'" % ("xxxxxxxxxxxxxxxxxx error para iniciar xxxxxxxxxxxxxxxxxx"))
            os.system("echo '%s'" % (e))


    def detener_ec2(self):
        ec2 = self.get_aws_client('ec2')
        #waiter = ec2.get_waiter('instance_stopped')
        try:
            response = ec2.stop_instances(InstanceIds=[self.instance_id], DryRun=False)
            os.system("echo '%s'" % (response))

           # os.system("echo '%s'" % ("------------waiting stop ec2----------"))
           # waiter.wait(
           #     InstanceIds=[
           #         self.instance_id,
           #     ],
           #     DryRun=False,
           #     WaiterConfig={
           #         'Delay': 3,
           #         'MaxAttempts': 20
           #     }
           # )
            os.system("echo '%s'" % ("------------instancia detenida----------"))

            self.instance_status = 'stopped'
            self.system_status = 'insufficient-data'
            self.estado = 'off'

        except ClientError as e:
            os.system("echo '%s'" % ("xxxxxxxxxxxxxxerror para detener instanciaxxxxxxxxxxxxxxxxx"))
            os.system("echo '%s'" % (e))


    def asociar_ip_elastica(self):
        ec2Client = self.get_aws_client('ec2')
        dryRun = False
        # Allocate an elastic IP
        eip = ec2Client.allocate_address(DryRun=dryRun, Domain='vpc')
        # Associate the elastic IP address with the instance launched above
        if eip:
            ec2Client.associate_address(
                DryRun=dryRun,
                InstanceId=self.instance_id,
                AllocationId=eip["AllocationId"])

            self.ip = eip["PublicIp"]
            self.es_ip_elastica = True

    def listo_recibir_comandos(self):
        ssm = self.get_aws_client('ssm')
        response = ssm.get_connection_status(
            Target= self.instance_id
        )
        os.system("echo '%s'" % (response))
        self.comando_status= response['Status']

    def execute_commands_on_linux_instances(self,client, commands, instance_ids):
        """Runs commands on remote linux instances
        :param client: a boto/boto3 ssm client
        :param commands: a list of strings, each one a command to execute on the instances
        :param instance_ids: a list of instance_id strings, of the instances on which to execute the command
        :return: the response from the send_command function (check the boto3 docs for ssm client.send_command() )
        """

        resp = client.send_command(
            DocumentName="AWS-RunShellScript",  # One of AWS' preconfigured documents
            Parameters={'commands': commands},
            InstanceIds=instance_ids,
        )
        return resp


    def instalar_bbb(self):
        if self.estado == 'on' and self.comando_status== 'connected':
            ssm = self.get_aws_client('ssm')

            commands = ['wget -qO- https://ubuntu.bigbluebutton.org/bbb-install.sh | bash -s -- -v xenial-22 -s '+self.record+' -e josebarrientosq@gmail.com']
            instance_ids = [self.instance_id]
            response = self.execute_commands_on_linux_instances(ssm, commands, instance_ids)
            os.system("echo '%s'" % (response))


    def get_apisecret(self):
        ssm = self.get_aws_client('ssm')

        commands = ['bbb-conf --secret']
        instance_ids = [self.instance_id]
        response = ssm.send_command(
            DocumentName="AWS-RunShellScript",  # One of AWS' preconfigured documents
            Parameters={'commands': commands},
            InstanceIds=instance_ids,
        )
        os.system("echo '%s'" % (response))

        command_id = response['Command']['CommandId']

        time.sleep(2)
        output = ssm.get_command_invocation(
            CommandId=command_id,
            InstanceId= self.instance_id,
        )
        os.system("echo '%s'" % (output))
        self.api_secret = output['StandardOutputContent'].split()[3]

    def instalar_bbb_webhooks(self):
        ssm = self.get_aws_client('ssm')

        commands = ['apt-get install bbb-webhooks']
        instance_ids = [self.instance_id]
        response = ssm.send_command(
            DocumentName="AWS-RunShellScript",  # One of AWS' preconfigured documents
            Parameters={'commands': commands},
            InstanceIds=instance_ids,
        )
        os.system("echo '%s'" % (response))

    def instalar_efs(self):
        ssm = self.get_aws_client('ssm')

        commands = ['apt-get install nfs-common']
        instance_ids = [self.instance_id]
        response = ssm.send_command(
            DocumentName="AWS-RunShellScript",  # One of AWS' preconfigured documents
            Parameters={'commands': commands},
            InstanceIds=instance_ids,
        )
        os.system("echo '%s'" % (response))

    def montar_efs(self):
        ssm = self.get_aws_client('ssm')
        file_videocam='var/kurento/recordings'
        commands = ['mount -t nfs4 -o nfsvers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=2,noresvport fs-2833e350.efs.us-east-2.amazonaws.com:/ '+ file_videocam]
        instance_ids = [self.instance_id]
        response = ssm.send_command(
            DocumentName="AWS-RunShellScript",  # One of AWS' preconfigured documents
            Parameters={'commands': commands},
            InstanceIds=instance_ids,
        )
        os.system("echo '%s'" % (response))

    def asociar(self):
        ssm = self.get_aws_client('ssm')
        response = ssm.create_association(
            Name='asociar',
            InstanceId= self.instance_id,
            DocumentVersion = '$DEFAULT',
            AssociationName='asociado'
        )
        os.system("echo '%s'" % (response))

    def ver_ssm(self):
        ssm = self.get_aws_client('ssm')
        response=ssm.describe_instance_information()
        os.system("echo '%s'" % (response))

    def respuesta_comando(self):
        ssm = self.get_aws_client('ssm')
        output = ssm.get_command_invocation(
            CommandId=self.command_id,
            InstanceId= self.instance_id,
        )
        os.system("echo '%s'" % ("respuesta ------------------------"))
        os.system("echo '%s'" % (output))

    def ver_zona(self):
        route53 = self.get_aws_client('route53')
        response = route53.list_hosted_zones_by_name()
        os.system("echo '%s'" % (response))

    def crear_zona(self):
        route53 = self.get_aws_client('route53')
        response = route53.create_hosted_zone(
            Name=self.zona,
            CallerReference=self.zona[:4],
            HostedZoneConfig={
                'Comment': 'comentario desde odoo',
                'PrivateZone': False
            },
        )
        os.system("echo '%s'" % (response))

    def crear_record(self):
        route53 = self.get_aws_client('route53')

        response = route53.change_resource_record_sets(
            HostedZoneId='Z093093132ENBV1TCQXVM',
            ChangeBatch={
                'Comment': 'comment',
                'Changes': [
                    {
                        'Action': 'UPSERT', #crea o actualiza
                        'ResourceRecordSet': {
                            'Name': self.record,
                            'Type': 'A',
                            'TTL': 300,
                            'ResourceRecords': [
                                {
                                    'Value': self.ip
                                },
                            ],
                        }
                    },
                ]
            }
        )
        os.system("echo '%s'" % (response))

    def corregir_errores_cambio_ip(self):
        ssm = self.get_aws_client('ssm')
        commands = ["cd /",
                    '''sed -i '302s/.*/<X-PRE-PROCESS cmd="set" data="external_rtp_ip='''+self.ip +'''"\/>/' /opt/freeswitch/conf/vars.xml''',
                    '''sed -i '315s/.*/<X-PRE-PROCESS cmd="set" data="external_sip_ip='''+self.ip+'''"\/>/' /opt/freeswitch/conf/vars.xml''',
                    '''sed - i '2s/.*/proxy_pass https:\/\/'''+self.ip+''':7443;/' / etc / bigbluebutton / nginx / sip.nginx''',
                    "ip addr add "+self.ip+"/32 dev lo",
                    '''sed -i '109s/.*/<param name="wss-binding"  value="'''+self.ip+''':7443"\/>/' /opt/freeswitch/conf/sip_profiles/external.xml''',
                    "bbb-conf --restart"
                    ]
        instance_ids = [self.instance_id]
        response = ssm.send_command(
            DocumentName="AWS-RunShellScript",  # One of AWS' preconfigured documents
            Parameters={'commands': commands},
            InstanceIds=instance_ids,
        )
        os.system("echo '%s'" % (response))



    def test_comando(self):
        ssm = self.get_aws_client('ssm')
        commands = ["mkdir /bbb-recorder/carpetatest"
                    ]
        instance_ids = [self.instance_id]
        response = ssm.send_command(
            DocumentName="AWS-RunShellScript",  # One of AWS' preconfigured documents
            Parameters={'commands': commands},
            InstanceIds=instance_ids,
        )
        os.system("echo '%s'" % (response))

        command_id = response['Command']['CommandId']

        time.sleep(2)
        output = ssm.get_command_invocation(
            CommandId=command_id,
            InstanceId=self.instance_id,
        )
        os.system("echo '%s'" % (output))

# -*- coding: utf-8 -*-
from odoo import http
import culqipy
import os


class PaymentCulqi(http.Controller):



    @http.route('/holamundo', auth='public')
    def holamundo(self, **kw):
        return http.request.render('payment_culqi.holamundo')

    @http.route('/culqi_test', auth='public')
    def culqi_checkout(self, **kw):
        return http.request.render('payment_culqi.culqi_checkout')

    @http.route('/culqi_pago', auth='public' ,method=["POST"], csrf=False)
    def culqi_pago(self, **post):
        os.system("echo '%s'" % "backend pago")
        token = post.get("token")
        monto = post.get("monto")
        culqipy.public_key = 'pk_test_211cef5e98db1c43'
        culqipy.secret_key = "sk_test_2Ib2jtbKGX0H147g"

        dir_charge = {"amount": monto, "currency_code": "PEN",
                      "email": "josebarrientosq@gmail.com",
                      "source_id": token
                      }
        charge =culqipy.Charge.create(dir_charge)
        os.system("echo '%s'" % (charge))
        if charge["object"] == "charge":
            if charge["outcome"]["type"] == "venta_exitosa":
                return http.request.render("payment_culqi.pago_exitoso")
        if charge["object"] == "error":
            return http.request.render("payment_culqi.pago_error")


#     @http.route('/payment_culqi/payment_culqi/objects/', auth='public')
#     def list(self, **kw):
#         return http.request.render('payment_culqi.listing', {
#             'root': '/payment_culqi/payment_culqi',
#             'objects': http.request.env['payment_culqi.payment_culqi'].search([]),
#         })

#     @http.route('/payment_culqi/payment_culqi/objects/<model("payment_culqi.payment_culqi"):obj>/', auth='public')
#     def object(self, obj, **kw):
#         return http.request.render('payment_culqi.object', {
#             'object': obj
#         })


# coding: utf-8

import json
import logging

import dateutil.parser
import pytz
from werkzeug import urls

from odoo import api, fields, models, _


_logger = logging.getLogger(__name__)


class AcquirerCulqi(models.Model):
    _inherit = 'payment.acquirer'

    provider = fields.Selection(selection_add=[('culqi', 'Culqi')])
    culqi_public_key = fields.Char("Clave Pública", required_if_provider='culqi', groups='base.group_user')
    culqi_secret_key = fields.Char("Clave Privada")

    def _get_culqi_urls(self, environment):
        """ Culqi URLs"""
        if environment == 'prod':
            return 'https://panel.culqi.com/#/desarrollo/llaves'
        return 'https://integ-panel.culqi.com/#/desarrollo/llaves'

    def culqi_get_form_action_url(self):
        self.ensure_one()
        environment = 'prod' if self.state == 'enabled' else 'test'
        return self._get_culqi_urls(environment)
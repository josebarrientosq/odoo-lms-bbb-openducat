console.log("-----------llave publica-----12----------")
// Configura tu llave pública
Culqi.publicKey = 'pk_test_211cef5e98db1c43';
// Configura tu Culqi Checkout
Culqi.settings({
    title: 'Culqi Store',
    currency: 'PEN',
    description: 'Polo Culqi lover',
    amount: 33000
});
// Usa la funcion Culqi.open() en el evento que desees
function pagar () {
    // Abre el formulario con las opciones de Culqi.settings
    console.log("-----------pagando----token--enviando--------")
    Culqi.open();
}

function culqi() {
    if (Culqi.token) { // ¡Objeto Token creado exitosamente!
        var token = Culqi.token.id;
        console.log("token creado")

        var data = {
                "token": token,
                "monto" : 5000,
        }

        $.post("/culqi_pago", data).done(function(response) {
            console.log("pago exitoso")
            console.log(response)
            $("#respuesta").html(response)
        })

      //En esta linea de codigo debemos enviar el "Culqi.token.id"
      //hacia tu servidor con Ajax
    } else { // ¡Hubo algún problema!
      // Mostramos JSON de objeto error en consola
      console.log(Culqi.error);
      alert(Culqi.error.user_message);
    }
}